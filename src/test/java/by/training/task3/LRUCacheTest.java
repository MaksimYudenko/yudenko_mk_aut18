package by.training.task3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LRUCacheTest {

    private LRUCache<Integer, Integer> cache;

    @Before
    public void setUp() {
        cache = new LRUCache<>(5);
        cache.put(2, 2);
        cache.put(4, 4);
        cache.put(1, 1);
        cache.put(3, 3);
        cache.put(5, 5);
    }

    @Test
    public void getNonexistentItem() {
        assertNull(cache.get(6));
    }

    @Test
    public void getAnItem() {
        assertEquals(Integer.valueOf(5), cache.get(5));
    }

    @Test
    public void putAnItem() {
        cache.remove(3);
        assertNull(cache.get(3));
        cache.put(3, 3);
        assertEquals(Integer.valueOf(3), cache.get(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void putAnItemIfAbsent() {
        cache.putIfAbsent(3, 33);
    }

    @Test
    public void removeAnItem() {
        assertEquals(Integer.valueOf(3), cache.get(3));
        cache.remove(3);
        assertNull(cache.get(3));
    }

    @Test
    public void whetherAnItemContains() {
        assertTrue(cache.contains(4));
        assertFalse(cache.contains(6));
    }

    @Test
    public void sizeEqualsCapacity() {
        assertEquals(5, cache.size());
    }

    @Test
    public void renewAnItem() {
        assertEquals(Integer.valueOf(3), cache.get(3));
        cache.put(3, 33);
        assertEquals(Integer.valueOf(33), cache.get(3));
    }

    @Test
    public void expulsionAnItemWhenCapacityExceeds() {
        cache.put(0, 0);
        assertNull(cache.get(2));
        assertEquals(Integer.valueOf(4), cache.get(4));
        cache.put(2, 2);
        assertNull(cache.get(1));
    }

}