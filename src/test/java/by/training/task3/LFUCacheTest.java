package by.training.task3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LFUCacheTest {

    private LFUCache<Integer, Integer> cache;

    @Before
    public void setUp() {
        cache = new LFUCache<>(4);
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        cache.put(4, 4);
    }

    @Test
    public void getNonexistentItem() {
        assertNull(cache.get(5));
        cache.put(5, 5);
        assertNull(cache.get(3));
    }

    @Test
    public void getAnItem() {
        assertEquals(Integer.valueOf(3), cache.get(3));
    }

    @Test
    public void putAnItem() {
        assertNull(cache.get(5));
        assertNull(cache.put(5, 5));
        assertEquals(Integer.valueOf(5), cache.get(5));
    }

    @Test
    public void putAnItemIfAbsent() {
        assertEquals(Integer.valueOf(33), cache.putIfAbsent(3, 33));
        assertNull(cache.putIfAbsent(5, 55));
        assertEquals(Integer.valueOf(55), cache.get(5));
    }

    @Test
    public void removeAnItem() {
        assertEquals(Integer.valueOf(4), cache.get(4));
        cache.remove(4);
        assertNull(cache.get(4));
    }

    @Test
    public void whetherAnItemContains() {
        assertTrue(cache.contains(4));
        assertFalse(cache.contains(5));
    }

    @Test
    public void sizeEqualsCapacity() {
        assertEquals(4, cache.size());
    }

    @Test
    public void renewAnItem() {
        assertEquals(Integer.valueOf(3), cache.get(3));
        cache.put(3, 33);
        assertEquals(Integer.valueOf(33), cache.get(3));
    }

    @Test
    public void expulsionAnItemWhenCapacityExceeds() {
        cache.put(5, 5);
        assertNull(cache.get(3));
    }

    @Test
    public void expulsionAnItemWithLowerFrequency() {
        assertTrue(cache.contains(1));
        assertFalse(cache.contains(5));
        cache.get(1);
        cache.get(3);
        cache.get(3);
        cache.get(1);
        cache.put(5, 5);
        assertTrue(cache.contains(1));
        assertFalse(cache.contains(3));
    }

}